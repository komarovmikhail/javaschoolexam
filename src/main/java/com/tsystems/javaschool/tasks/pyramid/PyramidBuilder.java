package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() >= Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);

        int amountOfNumbers = inputNumbers.size();
        int numberOfDigits = 0;
        while (amountOfNumbers > 0) {
            numberOfDigits++;
            amountOfNumbers = amountOfNumbers - numberOfDigits;
        }
        if (amountOfNumbers != 0) {
            throw new CannotBuildPyramidException();
        }

        int height = numberOfDigits;
        int width = 2 * numberOfDigits - 1;
        int[][] array = new int[height][width];
        int start = width / 2;
        int iterator = 0;
        for (int i = 0; i < array.length; i++) {
            int pos = start;
            for (int j = 0; j <= i; j++) {
                array[i][pos] = inputNumbers.get(iterator);
                pos += 2;
                iterator++;
            }
            start--;
        }
        return array;

    }

}
