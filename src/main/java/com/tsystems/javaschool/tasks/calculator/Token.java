package com.tsystems.javaschool.tasks.calculator;

/**
 * This class describes the meaningful elements of an expression.
 */
public class Token {
    private final TokenType type;
    private final String value;
    private int priority;

    public Token(TokenType type) {
        this.type = type;
        this.value = type.getValue();
        this.priority = type.getPriority();
    }

    public Token(TokenType type, String value) {
        this.type = type;
        this.value = value;
    }

    public TokenType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public int getPriority() {
        return priority;
    }
}
