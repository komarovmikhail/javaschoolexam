package com.tsystems.javaschool.tasks.calculator;

/**
 * Valid token types.
 */
public enum TokenType {
    OPERAND_NUMBER,
    OPERATOR_MUL("*", 2),
    OPERATOR_DIV("/", 2),
    OPERATOR_SUB("-", 1),
    OPERATOR_ADD("+", 1),
    LEFT_BRACKET("("),
    RIGHT_BRACKET(")");
    private int priority;
    private String value;

    TokenType(String value) {
        this.value = value;
    }

    TokenType(String value, int priority) {
        this.priority = priority;
        this.value = value;
    }

    TokenType() {
    }

    public int getPriority() {
        return priority;
    }

    public String getValue() {
        return value;
    }
}