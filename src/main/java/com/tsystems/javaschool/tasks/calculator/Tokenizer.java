package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.IncorrectOperandException;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class allows to implement a lexical analyzer.
 */
public class Tokenizer {
    /**
     * This method splits the original expression into separate parts - tokens.
     *
     * @param exp mathematical statement containing digits, '.' (dot) as decimal mark,
     *            parentheses, operations signs '+', '-', '*', '/'<br>
     * @return list of tokens preserving recording order
     * @throws IncorrectOperandException if invalid or incorrect character is found.
     */
    public List<Token> tokenListCreator(String exp) throws IncorrectOperandException {
        Scanner scanner = new Scanner(exp);
        String validationResult = scanner.findInLine("[^0.-9()+\\-*\\s\\/]+");

        if (validationResult != null) {
            throw new IncorrectOperandException();
        }

        int index = 0;
        List<Token> tokens = new ArrayList<>();
        String expression = exp.replace(" ", "");
        StringBuilder operand = new StringBuilder();
        while (index < expression.length()) {
            operand.setLength(0);
            char element = expression.charAt(index);
            while (Character.isDigit(element) || element == '.' || (index == 0 && element == '-') ||
                    (index > 0 && expression.charAt(index - 1) == '(' && element == '-')) {
                operand.append(element);
                index++;
                if (index == expression.length()) {
                    if (isOperandMatches(operand)) {
                        tokens.add(new Token(TokenType.OPERAND_NUMBER, operand.toString()));
                        break;
                    } else {
                        throw new IncorrectOperandException();
                    }
                }
                element = expression.charAt(index);
            }
            if (operand.length() != 0 && index != expression.length()) {
                if (isOperandMatches(operand)) {
                    tokens.add(new Token(TokenType.OPERAND_NUMBER, operand.toString()));
                } else {
                    throw new IncorrectOperandException();
                }
            }
            index++;
            switch (element) {
                case '*':
                    tokens.add(new Token(TokenType.OPERATOR_MUL));
                    break;
                case '/':
                    tokens.add(new Token(TokenType.OPERATOR_DIV));
                    break;
                case '-':
                    tokens.add(new Token(TokenType.OPERATOR_SUB));
                    break;
                case '+':
                    tokens.add(new Token(TokenType.OPERATOR_ADD));
                    break;
                case '(':
                    tokens.add(new Token(TokenType.LEFT_BRACKET, Character.toString(element)));
                    break;
                case ')':
                    tokens.add(new Token(TokenType.RIGHT_BRACKET, Character.toString(element)));
                    break;
            }
        }
        return tokens;
    }

    private boolean isOperandMatches(StringBuilder operand) {
        return operand.toString().matches("^\\d+\\.\\d+$") || operand.toString().matches("\\d+");
    }
}
