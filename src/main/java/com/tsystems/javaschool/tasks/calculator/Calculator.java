package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.IncorrectExpressionException;
import com.tsystems.javaschool.tasks.calculator.exception.IncorrectOperandException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

/**
 * A class for evaluating a mathematical expression represented as a string.
 */
public class Calculator {


    private final List<Token> queue = new ArrayList<>();
    private final Stack<Token> operators = new Stack<>();
    private final Tokenizer tokenizer = new Tokenizer();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.trim().length() == 0) {
            return null;
        }
        double result;
        try {
            List<Token> tokens = tokenizer.tokenListCreator(statement);
            List<Token> postfixTokens = toPostfixList(tokens);
            result = calculate(postfixTokens);
        } catch (EmptyStackException | IncorrectExpressionException | IncorrectOperandException e) {
            return null;
        }
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols();
        formatSymbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#.####", formatSymbols);
        return decimalFormat.format(result);
    }

    /**
     * This method realizes a "shunting-yard algorithm" - a method for parsing mathematical expressions
     * from infix notation into postfix notation.
     *
     * @param list of tokens in the original sequence
     * @return list of tokens in sorted (postfix notation) sequence
     * @throws IncorrectExpressionException if the mathematical expression is specified incorrectly
     * @throws EmptyStackException          if the mathematical expression is specified incorrectly.
     */
    private List<Token> toPostfixList(List<Token> list) throws IncorrectExpressionException, EmptyStackException {
        int index = 0;
        while (index < list.size()) {
            Token token = list.get(index);
            if (token.getType() == TokenType.OPERAND_NUMBER) {
                queue.add(token);
                index++;
            }
            if (token.getPriority() > 0) {
                if (!operators.isEmpty()) {
                    while (!operators.isEmpty() && operators.peek().getPriority() >= token.getPriority()) {
                        queue.add(operators.pop());
                    }
                }
                operators.push(token);
                index++;
            }
            if (token.getType() == TokenType.LEFT_BRACKET) {
                operators.push(token);
                index++;
            }
            if (token.getType() == TokenType.RIGHT_BRACKET) {
                while (operators.peek().getType() != TokenType.LEFT_BRACKET) {
                    queue.add(operators.pop());
                    if (operators.isEmpty()) {
                        throw new IncorrectExpressionException();
                    }
                }
                operators.pop();
                index++;
            }
            if (index == list.size()) {
                while (!operators.isEmpty()) {
                    if (operators.peek().getType() == TokenType.LEFT_BRACKET) {
                        throw new IncorrectExpressionException();
                    } else {
                        queue.add(operators.pop());
                    }
                }
            }
        }
        return queue;
    }

    /**
     * This method realizes the sequential placement of tokens into the resulting stack
     * with sequential execution of arithmetic operations on the first two operands from the resulting stack
     *
     * @param tokens in sorted (postfix notation) sequence
     * @return a double value as a result of performing arithmetic operations
     * @throws IncorrectExpressionException if math expression is invalid.
     */
    private double calculate(List<Token> tokens) throws IncorrectExpressionException {
        Stack<Token> result = new Stack<>();
        for (Token token : tokens) {
            if (token.getType() == TokenType.OPERAND_NUMBER) {
                result.push(token);
            } else {
                double num1 = Double.parseDouble(result.pop().getValue());
                double num2 = Double.parseDouble(result.pop().getValue());
                double res;
                if (token.getType() == TokenType.OPERATOR_ADD) {
                    res = num1 + num2;
                } else if (token.getType() == TokenType.OPERATOR_SUB) {
                    res = num2 - num1;
                } else if (token.getType() == TokenType.OPERATOR_MUL) {
                    res = num1 * num2;
                } else if (token.getType() == TokenType.OPERATOR_DIV) {
                    if (num1 == 0) {
                        throw new IncorrectExpressionException("num1 equals to zero");
                    }
                    res = num2 / num1;
                } else {
                    throw new IncorrectExpressionException("Unexpected token type");
                }
                result.push(new Token(TokenType.OPERAND_NUMBER, res + ""));
            }
        }
        return Double.parseDouble(result.pop().getValue());
    }

}
